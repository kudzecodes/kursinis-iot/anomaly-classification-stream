/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package lt.kudze.stream.anomaly.classification;

import lt.kudze.stream.anomaly.classification.messages.debezium.DebeziumValue;
import lt.kudze.stream.anomaly.classification.messages.device.*;
import lt.kudze.stream.anomaly.classification.messages.internal.InternalDeviceSensorValue;
import lt.kudze.stream.anomaly.classification.messages.internal.InternalDeviceValue;
import lt.kudze.stream.anomaly.classification.messages.output.OutputDeviceSensorAnomalyClassificationValue;
import lt.kudze.stream.anomaly.classification.topics.*;
import lt.kudze.stream.anomaly.classification.util.SystemUtil;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.KeyValueStore;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class AnomalyClassificationStream {

    public static KTable<DeviceKey, InternalDeviceValue> buildInternalDeviceTable(StreamsBuilder builder) {
        var debeziumDeviceStream = builder.stream(DebeziumDeviceTopic.NAME, DebeziumDeviceTopic.CONSUMED);

        //It is given that deviceUuid cannot change.
        return debeziumDeviceStream.filter(
                (key, value) -> value != null,
                Named.as("devices-filter-tombstone")
        ).mapValues(
                DebeziumValue::getPayload,
                Named.as("devices-map-payload")
        ).map(
                (key, value) -> new KeyValue<>(
                        key.getPayload(),
                        value.getAfter().map(after -> {
                            var modelUuid = after.getAnomalyModelUuid();
                            if (modelUuid == null)
                                return null;

                            return new InternalDeviceValue(modelUuid);
                        }).orElse(null)
                ),
                Named.as("devices-map-to-model")
        ).toTable(
                Named.as("devices-to-internal-table"),
                Materialized.<DeviceKey, InternalDeviceValue, KeyValueStore<Bytes, byte[]>>as("devices-internal-table")
                        .withKeySerde(DeviceKey.serde)
                        .withValueSerde(InternalDeviceValue.serde)
        );
    }

    public static void buildDeviceSensorTopology(StreamsBuilder builder) {
        var internalDeviceTable = buildInternalDeviceTable(builder);
        var deviceSensorStream = builder.stream(DeviceSensorTopic.NAME, DeviceSensorTopic.CONSUMED);

        deviceSensorStream.map(
                (key, value) -> new KeyValue<>(
                        new DeviceKey(key.getDeviceUuid()),
                        new InternalDeviceSensorValue(value, key.getDate())
                ),
                Named.as("devices-sensor-log-map-to-internal")
        ).join(
                internalDeviceTable,
                OutputDeviceSensorAnomalyClassificationValue::new,
                Joined.<DeviceKey, InternalDeviceSensorValue, InternalDeviceValue>as("device-sensor-log-join-to-device")
                        .withKeySerde(DeviceKey.serde)
                        .withValueSerde(InternalDeviceSensorValue.serde)
        ).to(
                OutputDeviceSensorAnomalyClassificationTopic.NAME,
                OutputDeviceSensorAnomalyClassificationTopic.PRODUCED
        );
    }

    public static StreamsBuilder buildTopology() {
        StreamsBuilder builder = new StreamsBuilder();

        buildDeviceSensorTopology(builder);

        return builder;
    }

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, SystemUtil.getenv("KAFKA_GROUP_ID"));
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, SystemUtil.getenv("KAFKA_BROKERS"));
        props.put(StreamsConfig.SECURITY_PROTOCOL_CONFIG, SystemUtil.getenv("KAFKA_SECURITY_PROTOCOL", "PLAINTEXT"));
        props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, SystemUtil.getenv("KAFKA_TRUSTSTORE_LOCATION", null));
        props.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, SystemUtil.getenv("KAFKA_TRUSTSTORE_PASSWORD", null));
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 100);
        props.put(StreamsConfig.producerPrefix(ProducerConfig.LINGER_MS_CONFIG), 0);

        final StreamsBuilder builder = AnomalyClassificationStream.buildTopology();

        final Topology topology = builder.build();
        System.out.println(topology.describe());

        final KafkaStreams streams = new KafkaStreams(topology, props);
        final CountDownLatch latch = new CountDownLatch(1);

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            streams.start();
            latch.await();
        } catch (Throwable e) {
            System.exit(1);
        }
        System.exit(0);
    }
}
