package lt.kudze.stream.anomaly.classification.messages.device;

import lt.kudze.stream.anomaly.classification.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DeviceValue {
    public static final Serde<DeviceValue> serde = JsonSerdes.of(DeviceValue.class);

    private String anomalyModelUuid;

    public DeviceValue() {

    }

    public String getAnomalyModelUuid() {
        return anomalyModelUuid;
    }

    public void setAnomalyModelUuid(String anomalyModelUuid) {
        this.anomalyModelUuid = anomalyModelUuid;
    }
}
