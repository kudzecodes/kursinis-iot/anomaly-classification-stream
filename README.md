# Anomaly Classification Stream

This is kafka stream that takes in live device sensor logs and device data from user-devices-rest, 
and produces messages for anomaly classification.
