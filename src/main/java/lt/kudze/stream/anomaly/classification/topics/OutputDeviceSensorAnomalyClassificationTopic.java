package lt.kudze.stream.anomaly.classification.topics;

import lt.kudze.stream.anomaly.classification.messages.device.DeviceKey;
import lt.kudze.stream.anomaly.classification.messages.output.OutputDeviceSensorAnomalyClassificationValue;
import lt.kudze.stream.anomaly.classification.util.SystemUtil;
import org.apache.kafka.streams.kstream.Produced;

public class OutputDeviceSensorAnomalyClassificationTopic {
    public static final String NAME = SystemUtil.getenv("KAFKA_OUTPUT_DEVICE_SENSOR_ANOMALY_CLASSIFICATION_TOPIC");
    public static final Produced<DeviceKey, OutputDeviceSensorAnomalyClassificationValue> PRODUCED = Produced.with(DeviceKey.serde, OutputDeviceSensorAnomalyClassificationValue.serde);
}
