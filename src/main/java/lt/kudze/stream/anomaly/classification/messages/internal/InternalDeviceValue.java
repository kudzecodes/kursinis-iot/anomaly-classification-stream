package lt.kudze.stream.anomaly.classification.messages.internal;

import lt.kudze.stream.anomaly.classification.serdes.StringSerdes;
import org.apache.kafka.common.serialization.Serde;

public class InternalDeviceValue {
    public static final Serde<InternalDeviceValue> serde = StringSerdes.from(
            InternalDeviceValue::toString,
            InternalDeviceValue::new
    );

    private String anomalyModelUuid;

    public InternalDeviceValue() {

    }

    public InternalDeviceValue(String anomalyModelUuid) {
        this.anomalyModelUuid = anomalyModelUuid;
    }

    public String getAnomalyModelUuid() {
        return anomalyModelUuid;
    }

    public void setAnomalyModelUuid(String anomalyModelUuid) {
        this.anomalyModelUuid = anomalyModelUuid;
    }

    @Override
    public String toString() {
        return this.anomalyModelUuid;
    }
}
