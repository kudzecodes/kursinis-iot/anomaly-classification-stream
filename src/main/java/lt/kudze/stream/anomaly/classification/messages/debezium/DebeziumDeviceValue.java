package lt.kudze.stream.anomaly.classification.messages.debezium;

import lt.kudze.stream.anomaly.classification.messages.device.DeviceValue;
import lt.kudze.stream.anomaly.classification.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DebeziumDeviceValue extends DebeziumValue<DeviceValue> {
    public static final Serde<DebeziumDeviceValue> serde = JsonSerdes.of(DebeziumDeviceValue.class);
}
