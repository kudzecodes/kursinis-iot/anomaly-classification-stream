package lt.kudze.stream.anomaly.classification.messages.debezium;

import lt.kudze.stream.anomaly.classification.messages.device.DeviceKey;
import lt.kudze.stream.anomaly.classification.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DebeziumDeviceKey extends DebeziumKey<DeviceKey> {
    public static final Serde<DebeziumDeviceKey> serde = JsonSerdes.of(DebeziumDeviceKey.class);
}
