package lt.kudze.stream.anomaly.classification.messages.output;

import lt.kudze.stream.anomaly.classification.messages.internal.InternalDeviceSensorValue;
import lt.kudze.stream.anomaly.classification.messages.internal.InternalDeviceValue;
import lt.kudze.stream.anomaly.classification.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class OutputDeviceSensorAnomalyClassificationValue extends InternalDeviceSensorValue {
    public static final Serde<OutputDeviceSensorAnomalyClassificationValue> serde = JsonSerdes.of(OutputDeviceSensorAnomalyClassificationValue.class);
    private String anomalyModelUuid;

    public OutputDeviceSensorAnomalyClassificationValue() {

    }

    public OutputDeviceSensorAnomalyClassificationValue(
            InternalDeviceSensorValue value,
            InternalDeviceValue deviceValue
    ) {
        super(value);

        this.anomalyModelUuid = deviceValue.getAnomalyModelUuid();
    }

    public String getAnomalyModelUuid() {
        return anomalyModelUuid;
    }

    public void setAnomalyModelUuid(String anomalyModelUuid) {
        this.anomalyModelUuid = anomalyModelUuid;
    }
}
