import lt.kudze.stream.anomaly.classification.AnomalyClassificationStream;
import lt.kudze.stream.anomaly.classification.messages.device.DeviceKey;
import lt.kudze.stream.anomaly.classification.messages.device.DeviceSensorKey;
import lt.kudze.stream.anomaly.classification.messages.device.DeviceSensorValue;
import lt.kudze.stream.anomaly.classification.messages.output.OutputDeviceSensorAnomalyClassificationValue;
import lt.kudze.stream.anomaly.classification.topics.DebeziumDeviceTopic;
import lt.kudze.stream.anomaly.classification.topics.DeviceSensorTopic;
import lt.kudze.stream.anomaly.classification.topics.OutputDeviceSensorAnomalyClassificationTopic;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import resources.input.InputDevices;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class AnomalyClassificationStreamTest {

    @Test
    public void testEndToEnd() throws IOException {
        System.out.println("Setup topology driver");
        Properties config = new Properties();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "test");
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9999"); // not existing server
        config.put(StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG,
                org.apache.kafka.streams.errors.LogAndContinueExceptionHandler.class.getName()
        );

        var topology = AnomalyClassificationStream.buildTopology().build();
        System.out.println(topology.describe());

        TopologyTestDriver testDriver = new TopologyTestDriver(topology, config);
        //testDriver.close();

        var inputDeviceTopic = testDriver.createInputTopic(DebeziumDeviceTopic.NAME, Serdes.String().serializer(), Serdes.String().serializer());
        var inputDeviceSensorLogTopic = testDriver.createInputTopic(DeviceSensorTopic.NAME, DeviceSensorKey.serde.serializer(), DeviceSensorValue.serde.serializer());
        var outputTopic = testDriver.createOutputTopic(OutputDeviceSensorAnomalyClassificationTopic.NAME, DeviceKey.serde.deserializer(), OutputDeviceSensorAnomalyClassificationValue.serde.deserializer());

        var testLogKey = new DeviceSensorKey("9c067be6-a93d-403b-88e3-c3b84bfc2618", "date");
        var testLogValue = new DeviceSensorValue("json");

        Assertions.assertTrue(outputTopic.isEmpty());

        //If no device then no output.
        inputDeviceSensorLogTopic.pipeInput(testLogKey, testLogValue);
        Assertions.assertTrue(outputTopic.isEmpty());

        //If we add device we should see output.
        inputDeviceTopic.pipeInput(
                getResource(InputDevices.FACTORY_KEY),
                getResource(InputDevices.FACTORY_VALUE)
        );
        inputDeviceSensorLogTopic.pipeInput(testLogKey, testLogValue);
        var record = outputTopic.readRecord();
        Assertions.assertEquals(testLogKey.getDeviceUuid(), record.key().getUuid());
        Assertions.assertEquals(testLogKey.getDate(), record.value().getDate());
        Assertions.assertEquals(testLogValue.getJson(), record.value().getJson());
        Assertions.assertEquals("9c067be6-a93d-403b-88e3-c3b84bfc2618", record.value().getAnomalyModelUuid());
        Assertions.assertTrue(outputTopic.isEmpty());

        //If we add device without model we should not see output.
        inputDeviceTopic.pipeInput(
                getResource(InputDevices.FACTORY_KEY),
                getResource(InputDevices.FACTORY_NO_MODEL_VALUE)
        );
        inputDeviceSensorLogTopic.pipeInput(testLogKey, testLogValue);
        Assertions.assertTrue(outputTopic.isEmpty());

        //If we delete device we should not see output.
        inputDeviceTopic.pipeInput(
                getResource(InputDevices.FACTORY_KEY),
                getResource(InputDevices.FACTORY_DELETE_VALUE)
        );
        inputDeviceSensorLogTopic.pipeInput(testLogKey, testLogValue);
        Assertions.assertTrue(outputTopic.isEmpty());

        //Same after tombstone.
        inputDeviceTopic.pipeInput(
                getResource(InputDevices.FACTORY_KEY),
                "null"
        );
        inputDeviceSensorLogTopic.pipeInput(testLogKey, testLogValue);
        Assertions.assertTrue(outputTopic.isEmpty());

        testDriver.close();
    }

    private String getResource(String resource) throws IOException {
        return Files.readString(Path.of(Objects.requireNonNull(this.getClass().getResource("/messages/" + resource)).getPath()));
    }
}
