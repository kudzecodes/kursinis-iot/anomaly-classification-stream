package lt.kudze.stream.anomaly.classification.topics;

import lt.kudze.stream.anomaly.classification.messages.device.DeviceSensorKey;
import lt.kudze.stream.anomaly.classification.messages.device.DeviceSensorValue;
import lt.kudze.stream.anomaly.classification.util.SystemUtil;
import org.apache.kafka.streams.kstream.Consumed;

public class DeviceSensorTopic {
    public static final String NAME = SystemUtil.getenv("KAFKA_DEVICE_SENSOR_TOPIC");
    public static final Consumed<DeviceSensorKey, DeviceSensorValue> CONSUMED = Consumed.with(DeviceSensorKey.serde, DeviceSensorValue.serde);
}
