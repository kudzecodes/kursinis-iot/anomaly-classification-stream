package resources.input;

public class InputDevices {
    public static final String FACTORY_KEY = "input/devices/factory.key.json";
    public static final String FACTORY_VALUE = "input/devices/factory.json";
    public static final String FACTORY_NO_MODEL_VALUE = "input/devices/factoryNoModel.json";
    public static final String FACTORY_DELETE_VALUE = "input/devices/factoryDelete.json";
}
